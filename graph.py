# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 13:11:33 2024

@author: ahcene
"""

# ----------------------------------les import 
import tkinter as tk 
#----------------------------------- 

COULEURS=["red","blue","orange","yellow","purple","pink"] 
gris =["gray","gray","gray","gray"]
def new_widow ():
    frame.pack_forget()
    frame_sec.pack()
    

def open_color_window(index):
    if not hasattr(window, 'color_window'):
        color_window = tk.Toplevel(window)
        color_window.geometry("200x300")
        color_window.title("Choisir une couleur")

        for couleur in COULEURS:
            color_button = tk.Button(color_window, bg=couleur, width=10, height=2, command=lambda c=couleur, i=index: update_gris_color(c, i))
            color_button.pack(pady=5)  # Placeholder for actual game logic  
    
    window.color_window.deiconify()
    

def update_gris_color(couleur, index):
    # Mettre à jour la couleur du bouton gris correspondant
    gris_buttons[index].config(bg=couleur)
    
#creation de la fenetre
#---------------------------------- ----------------- 
window = tk.Tk()
window.iconbitmap("C:/Users/ahcen/OneDrive/Bureau/prog_info/projet_master_mind/mastermind.ico")
window.geometry("1920x1080")
window.config(background='#D9AFA7')

#- Frame 1
#---------------------------------------------------- 

frame = tk.Frame(window, bg = '#D9AFA7')

title = tk.Label(frame,text = "MASTER MIND",font=("Impact",40), bg = '#D9AFA7', fg = 'black')
title.pack()

subtitle = tk.Label(frame,text = "bienvenue dans mon jeux,  voulez vous y jouez ?",font=("Impact",20), bg ='#D9AFA7', fg = 'black')
subtitle.pack()

play_bouton = tk.Button(frame,text = "jouez",font=("Impact",20), bg ='#D9AFA7', fg = 'black',command = new_widow)
play_bouton.pack() 

frame.pack(expand = tk.YES)

#- Frame 2
#----------------------------------------------------

frame_sec = tk.Frame(window, bg = '#D9AFA7')
title_play = tk.Label(frame_sec,text = "MASTER MIND",font=("Impact",40), bg = '#D9AFA7', fg = 'black')  
title_play.pack()   

canvas = tk.Canvas(frame_sec, width=400, height=400, bg='white')
canvas.pack()

#les boutons
gris_buttons=[]
for index,couleur in enumerate(gris):
    color_button = tk.Button(frame_sec, bg=couleur, width=2, command=lambda i=index: open_color_window(i))
    #color_button = tk.Button(frame_sec, bg=couleur, width=2, command = open_color_window)
    color_button.pack(side=tk.RIGHT, padx=10)
    gris_buttons.append(color_button)
#----------------------------------------------------

window.mainloop()